package QDTS;

import org.junit.jupiter.api.*;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for Exercise11.
 */

@DisplayName("11th exercise Test")
public class OnlineMediaTest {
    private final static PipedOutputStream pipe = new PipedOutputStream();
    private final static ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final static ByteArrayOutputStream err = new ByteArrayOutputStream();

    private final static InputStream originalIn = System.in;
    private final static PrintStream originalOut = System.out;
    private final static PrintStream originalErr = System.err;

    @BeforeAll
    public static void catchIO() throws IOException {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
        System.setIn(new PipedInputStream(pipe));
    }

    @AfterAll
    public static void releaseIO() {
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.setIn(originalIn);
    }


    @Test
    @DisplayName("11th exercise Test")
    public void OnlineMediaTestMain() throws Exception {

        OnlineMedia.main(new String[0]);
        String[] output = out.toString().split(System.lineSeparator());
        assertNotNull(output[0]);
        //assertEquals("dvd1.title = IBM Dance Party", output[0]);

        assertNotNull(output[1]);
        //assertEquals("Playing DVD: IBM Dance Party", output[1]);

        assertNotNull(output[2]);
        //assertEquals("DVD length: 87", output[2]);

        assertNotNull(output[3]);
        //assertEquals("[The cost of IBM Dance Party has increased from $19.95 to $22.22 ]", output[3]);

        assertNotNull(output[4]);
        //assertEquals("[The cost of IBM Dance Party has increased from $19.95 to $22.22 ]", output[4]);
        assertNotNull(output[5]);
        //assertEquals("[The cost of IBM Dance Party has decreased from $22.22 to $14.67 ]", output[5]);

        assertNotNull(output[6]);
        //assertEquals("Playing DVD: IBM Dance Party", output[6]);

        assertNotNull(output[7]);
        //assertEquals("DVD length: 87", output[7]);

        assertNotNull(output[8]);
        //assertEquals("Playing DVD: IBM Dance Party", output[8]);

        assertNotNull(output[9]);
        //assertEquals("DVD length: 87", output[9]);

        assertNotNull(output[10]);
        //assertEquals("dvd2.title = IBM Kids Sing-along", output[10]);

        assertNotNull(output[11]);
        //assertEquals("Playing DVD: IBM Kids Sing-along", output[11]);

        assertNotNull(output[12]);
        //assertEquals("DVD length: 124", output[12]);

        assertNotNull(output[13]);
        //assertEquals("Playing DVD: IBM Kids Sing-along", output[13]);

        assertNotNull(output[14]);
        //assertEquals("DVD length: 124", output[14]);

        assertNotNull(output[15]);
        //assertEquals("dvd3.title = IBM Smarter Planet", output[15]);

        assertNotNull(output[16]);
        //assertEquals("Playing DVD: IBM Smarter Planet", output[16]);

        assertNotNull(output[17]);
        //assertEquals("DVD length: 90", output[17]);

        assertNotNull(output[18]);
        //assertEquals("Playing DVD: IBM Smarter Planet", output[18]);

        assertNotNull(output[19]);
        //assertEquals("DVD length: 90", output[19]);

        assertNotNull(output[20]);
        //assertEquals("Book title = Java Programming", output[20]);

        assertNotNull(output[21]);
        //assertEquals("cd1.title = IBM Symphony", output[21]);

        assertNotNull(output[22]);
        //assertEquals("Cost of CD = 29.95", output[22]);

        assertNotNull(output[23]);
        //assertEquals("------------------------------------", output[23]);

        assertNotNull(output[24]);
        //assertEquals("The tracks in sorted order are: ", output[24]);

        assertNotNull(output[25]);
        //assertEquals("Playing DVD: IBM Kids Sing-along", output[25]);

        assertNotNull(output[26]);

        //assertEquals("------------------------------------", output[27]);
        assertNotNull(output[28]);
        //assertEquals("The tracks currently are in the order: ", output[28]);

        assertNotNull(output[29]);
        //assertEquals("t1.title = Warmup", output[29]);

        assertNotNull(output[30]);
        //assertEquals("t2.title = Scales", output[30]);

        assertNotNull(output[31]);
        //assertEquals("t3.title = Introduction", output[31]);

        //assertEquals("------------------------------------", output[32]);

        assertNotNull(output[33]);
        //assertEquals("Playing CD: IBM Symphony", output[33]);

        assertNotNull(output[34]);
        //assertEquals("CD length:10", output[34]);

        assertNotNull(output[35]);
        //assertEquals("Playing Track: Warmup", output[35]);

        assertNotNull(output[36]);
        //assertEquals("Track length: 3", output[36]);

        assertNotNull(output[37]);
        //assertEquals("Playing Track: Scales", output[37]);

        assertNotNull(output[38]);
        //assertEquals("Track length: 4", output[38]);

        assertNotNull(output[39]);
        //assertEquals("Playing Track: Introduction", output[39]);

        assertNotNull(output[40]);
        //assertEquals("Track length: 3", output[40]);

        assertNotNull(output[50]);
        //assertEquals("New Order's total cost is: 158.55", output[50]);
    }
}
