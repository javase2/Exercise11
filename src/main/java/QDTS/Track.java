package QDTS;

public class Track implements Playable,
		Comparable<Track> {
	private String title;
	private int length;
	public Track() {
		
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERROR: Track length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing Track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}
	public int compareTo(Track t2) {
		return (this.getTitle()).compareTo(t2.getTitle());
	} 
//	public int compareTo(Track t2) {
//		return Float.compare(this.getLength(), t2.getLength());
//	} 
}
