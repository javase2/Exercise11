package QDTS;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class OnlineMedia {

	public OnlineMedia() {
		super();
	}

	public static void main(String[] args) 
		throws FileNotFoundException {
		
		Thread memDaemon = new Thread(new MemoryDaemon());
		memDaemon.setDaemon(true);
		memDaemon.start();		
		
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream("./src/main/resources/media.properties");
	    	try {
	    		prop.load(fis);
	   	 } catch(IOException e) {
		}
		// Create a new object for getting property data
		DataFromProperties data = new DataFromProperties();
		
		// Create a new Order object
		Order anOrder = new Order();
		
		// add a number of dvds to the order
		int dvdsToBeAdded = 3;
		for (int dvdNumber=1; dvdNumber <= dvdsToBeAdded; dvdNumber++){
			// get the dvd data from the properties file
			DigitalVideoDisc dvd = data.addADvd(prop, dvdNumber);
			if (dvdNumber == 1) {
				MediaPriceChangeListener mpl = new MediaPriceChangeListener();
				//Register the new listener with dvd1
				dvd.addPropertyChangeListener(mpl);
				//Increase the cost of dvd1
				dvd.setCost(22.22f);
				//Decrease the cost of dvd1
				dvd.setCost(14.67f);
			}
			// add the dvd to the order
			try {
				dvd.play();
			} catch(Exception e) {
				e.printStackTrace();
			};
			anOrder.addMedia(dvd);
		}
		
		// add a book to the order
		Book book = new Book();
		book.setTitle("Java Programming");
		book.setCategory("Java");
		book.setCost(69.99f);
		book.addAuthor("Joe Wiggleworth");
		book.addAuthor("Paula McMillan");
		System.out.println("Book title = " + book.getTitle());
		   anOrder.addMedia(book);

	    // add a cd to the order
		int cdNumber = 1;
		int tracksToBeAdded = 3;
		CompactDisc cd = data.addACd(prop, cdNumber);
		System.out.println("------------------------------------");
		MediaCategoryChangeListener mcl = new MediaCategoryChangeListener();
		cd.addVetoableChangeListener(mcl);
		book.addVetoableChangeListener(mcl);
		cd.setCategory("Heavy metal");
		book.setCategory("Mystery");
		Library<Track> mt = new Library<Track>();
		Track track = mt.getMediaType();
		System.out.println("------------------------------------");
		System.out.println ("The tracks currently are in the order: ");
		for (int i=1; i <= tracksToBeAdded; i++){
			// get the track data from the properties file
			track = data.addATrack(prop, i);
			//cd.addTrack(track);
			mt.addMedia(track);
		}
		System.out.println("------------------------------------");
		cd.setTracks(mt.getVariable());
		try {
			cd.play();
		} catch(Exception e) {
			e.printStackTrace();
		};

		java.util.Collection<Track> collection = cd.getTracks();
		java.util.Collections.sort((java.util.ArrayList<Track>)collection);
		System.out.println("------------------------------------");
		System.out.println ("The tracks in sorted order are: ");
		// Iterate through the collection and output their titles 
		// in sorted order
		for (Track trackItem: collection) {
			System.out.println
			(trackItem.getTitle());
		}
		System.out.println("------------------------------------");
		anOrder.addMedia(cd);
		System.out.print("The total length of the CD is ");
		System.out.println(cd.getLength());
		System.out.print("Total Cost of the Order is: ");
		System.out.println(anOrder.totalCost());
	}
}
