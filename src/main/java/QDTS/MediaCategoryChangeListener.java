package QDTS;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

public class MediaCategoryChangeListener implements VetoableChangeListener {

	public MediaCategoryChangeListener() {
		
	}

	public void vetoableChange(PropertyChangeEvent evt)
			throws PropertyVetoException {
		String oldvalue = (String) evt.getOldValue();
		String newvalue = (String) evt.getNewValue();
		if (evt.getSource() instanceof CompactDisc) {
			System.out.println("[The " + evt.getPropertyName()
					+ " of this item has been changed from " + oldvalue
					+ " to " + newvalue + " ]");
		} else {
			throw new PropertyVetoException(
					"[Sorry - the category for this media type can not be changed.]",
					evt);
		}
	}

}
