package QDTS;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class MediaPriceChangeListener implements PropertyChangeListener {

	public MediaPriceChangeListener() {
		
	}

	public void propertyChange(PropertyChangeEvent evt) {
		float oldvalue = ((Float) evt.getOldValue()).floatValue();
		float newvalue = ((Float) evt.getNewValue()).floatValue();

		if (oldvalue < newvalue) {
			System.out.println("[The " + evt.getPropertyName() + " of "
					+ ((Media) evt.getSource()).getTitle()
					+ " has increased from $" + oldvalue + " to $" + newvalue
					+ " ]");
		} else {
			System.out.println("[The " + evt.getPropertyName() + " of "
					+ ((Media) evt.getSource()).getTitle()
					+ " has decreased from $" + oldvalue + " to $" + newvalue
					+ " ]");
		}
	}

}
