package QDTS;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;

public class Media {
	private String title;
	private String category = "No category";
	private float cost;
	private PropertyChangeSupport changes = 
	      new PropertyChangeSupport(this);
	private VetoableChangeSupport vetoes = 
	      new VetoableChangeSupport(this);
	public Media() {
		super();
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		String oldCategory = new String(this.category);
		this.category = category;
		try {
			vetoes.fireVetoableChange("category", oldCategory, this.category);
		} catch (PropertyVetoException e) {
			
			System.out.println(e.getMessage());
		} 
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		Float oldCost = new Float (this.cost);
		this.cost = cost;
		changes.firePropertyChange("cost", oldCost, 
		new Float(this.cost));
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void addPropertyChangeListener(PropertyChangeListener p) {
		changes.addPropertyChangeListener(p);
	}

	public void removePropertyChangeListener(PropertyChangeListener p) {
		changes.removePropertyChangeListener(p);
	}
	public void addVetoableChangeListener(VetoableChangeListener v) {
		vetoes.addVetoableChangeListener(v);
	}

	public void removeVetoableChangeListener(VetoableChangeListener v) {
		vetoes.removeVetoableChangeListener(v);
	}
}
